//
//  ViewController.swift
//  ColourMix
//
//  Created by Sampada Sakpal on 1/8/20.
//  Copyright © 2020 Sampada Sakpal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateColour()
        updateControls()
        colourView.layer.borderWidth = 5
        colourView.layer.cornerRadius = 20
        colourView.layer.borderColor = UIColor.black.cgColor
    }
    
    func updateControls() {
        redSlider.isEnabled = redSwitch.isOn
        greenSlider.isEnabled = greenSwitch.isOn
        blueSlider.isEnabled = blueSwitch.isOn
    }
    func updateColour() {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        if redSwitch.isOn {
            red = CGFloat(redSlider.value)
        }
        if greenSwitch.isOn {
            green = CGFloat(greenSlider.value)
        }
        if blueSwitch.isOn {
            blue = CGFloat(blueSlider.value)
        }
        let colour = UIColor(red: red, green: green, blue: blue, alpha: 1)
        colourView.backgroundColor = colour
       
    }

    @IBOutlet weak var colourView: UIView!
    
    @IBOutlet weak var redSwitch: UISwitch!
    
    @IBOutlet weak var greenSwitch: UISwitch!
    
    @IBOutlet weak var blueSwitch: UISwitch!
    
    @IBOutlet weak var redSlider: UISlider!
    
    @IBOutlet weak var greenSlider: UISlider!
    
    @IBOutlet weak var blueSlider: UISlider!
    
    @IBAction func sliderChanged(_ sender: Any) {
        updateColour()
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        updateColour()
        updateControls()
    }
  
    @IBAction func reset(_ sender: AnyObject) {
        blueSwitch.isOn = false
        redSwitch.isOn = false
        greenSwitch.isOn = false
        
        redSlider.value = 1
        greenSlider.value = 1
        blueSlider.value = 1
        updateColour()
        updateControls()
    }
}

